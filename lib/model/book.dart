class Book {
  final int id;
  final String title;
  final String author;
  final String urlImage;
  final double price;
  final String language;

  const Book(
      {required this.id,
      required this.author,
      required this.title,
      required this.urlImage,
      required this.price,
      required this.language});

  factory Book.fromJson(Map<String, dynamic> json) => Book(
      id: json['id'],
      author: json['author'],
      title: json['title'],
      urlImage: json['cover'],
      price: double.parse(json['price']),
      language: json['language']);

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
        'author': author,
        'urlImage': urlImage,
      };
}
