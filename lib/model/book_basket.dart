import 'package:book_app/model/book.dart';
import 'package:flutter/material.dart';

class BookBasket extends ChangeNotifier {
  List<Book> books = [];
  double total = 0;

  void addBook(Book book) {
    books.add(book);
    total+=book.price;
    notifyListeners();
  }

  int get bookLength {
    return books.length;
  }

  double get priceBooks {
    return total;
  }

  List<Book> get totalBooks {
    return books;
  }

  void deleteBook(Book book) {
    books.remove(book);
    total-=book.price;
    notifyListeners();
  }
}
