import 'package:book_app/controller/book_controller.dart';
import 'package:book_app/model/book.dart';
import 'package:book_app/model/book_basket.dart';
import 'package:book_app/view/book_cart.dart';
import 'package:book_app/view/widget/profile.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BookDetails extends StatefulWidget {
  final Book book;

  const BookDetails({Key? key, required this.book}) : super(key: key);

  @override
  _BookDetailsState createState() => _BookDetailsState();
}

class _BookDetailsState extends State<BookDetails> {
  List<Book> books = [];
  String query = '';

  Future init() async {
    final books = await BooksController.getBooks(query);

    setState(() => this.books = books);
  }

  @override
  void initState() {
    init();

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Profile(
                  name: 'Saleh Souayh',
                  goBack: true,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const BookCart()),
                      );
                    },
                    child: Row(
                      children: [
                        const Icon(Icons.shopping_cart_sharp),
                        Consumer<BookBasket>(
                          builder: (context, basket, child) {
                            return Text(basket.total.toStringAsFixed(2) + "\$");
                          },
                        ),
                        const SizedBox(
                          width: 16,
                        )
                      ],
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Expanded(
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                          child: Text(
                            widget.book.title,
                            style: const TextStyle(
                                fontWeight: FontWeight.w400, fontSize: 25),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                          child: Text(
                            widget.book.author,
                            style: const TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: 20,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text("Language : " + widget.book.language),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Price : " + widget.book.price.toString()),
                            Consumer<BookBasket>(
                              builder: (context, basket, child) {
                                return IconButton(
                                    onPressed: () {
                                      basket.addBook(widget.book);
                                    },
                                    icon: const Icon(
                                        Icons.add_shopping_cart_sharp));
                              },
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Image.network(widget.book.urlImage,
                            width: 300, height: 450, fit: BoxFit.fill),
                        const SizedBox(
                          height: 20,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Row(
                            children: const [
                              Text(
                                "Other books",
                                style: TextStyle(
                                    fontSize: 17, fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget bookCard(Book book) => Container(
        padding: const EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: const [
            BoxShadow(
              color: Colors.grey,
              offset: Offset(0.0, 1.0), //(x,y)
              blurRadius: 6.0,
            ),
          ],
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.network(
              book.urlImage,
              fit: BoxFit.fill,
              width: 70,
              height: 70,
            ),
            const SizedBox(
              width: 5,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    book.title,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 15),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(book.author),
                  Text(
                    book.price.toString() + '\$',
                    style: const TextStyle(fontSize: 13),
                  ),
                ],
              ),
            ),
            Consumer<BookBasket>(builder: (context, basket, child) {
              return IconButton(
                  onPressed: () {
                    basket.addBook(book);
                  },
                  icon: const Icon(Icons.add_shopping_cart_sharp));
            }),
          ],
        ),
      );
}
