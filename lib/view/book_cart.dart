import 'package:book_app/model/book.dart';
import 'package:book_app/model/book_basket.dart';
import 'package:book_app/view/widget/profile.dart';
import 'package:book_app/view/widget/search_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BookCart extends StatefulWidget {
  const BookCart({Key? key}) : super(key: key);

  @override
  _BookCartState createState() => _BookCartState();
}

class _BookCartState extends State<BookCart> {
  List<Book> books = [];
  String query = '';

  searchBook(String query) {
    List<Book> booksBasket =
        Provider.of<BookBasket>(context, listen: false).books;

    setState(() {
      books = booksBasket.where((book) {
        final titleLower = book.title.toLowerCase();
        final authorLower = book.author.toLowerCase();
        final searchLower = query.toLowerCase();

        return titleLower.contains(searchLower) ||
            authorLower.contains(searchLower);
      }).toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Profile(
                  name: 'Saleh Souayh',
                  goBack: true,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Row(
                  children: [
                    Expanded(child: buildSearch()),
                    Row(
                      children: [
                        const Icon(Icons.shopping_cart_sharp),
                        Consumer<BookBasket>(
                          builder: (context, basket, child) {
                            return Text(
                                basket.total.toStringAsFixed(2) + "\$");
                          },
                        ),
                        const SizedBox(
                          width: 16,
                        )
                      ],
                    )
                  ],
                ),
              ),
              Consumer<BookBasket>(
                builder: (context, basket, child) {
                  return Expanded(
                      child: basket.bookLength > 0
                          ? ListView.builder(
                              itemCount: books.isNotEmpty
                                  ? books.length
                                  : basket.bookLength,
                              itemBuilder: (BuildContext context, int index) {
                                return Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: bookCard(books.isNotEmpty
                                      ? books[index]
                                      : basket.books[index]),
                                );
                              })
                          : const Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Center(child: Text("Cart Empty"))));
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget buildSearch() => SearchWidget(
        text: query,
        hintText: 'Title or Author Name',
        onChanged: searchBook,
      );

  Widget bookCard(Book book) => Container(
        padding: const EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: const [
            BoxShadow(
              color: Colors.grey,
              offset: Offset(0.0, 1.0), //(x,y)
              blurRadius: 6.0,
            ),
          ],
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.network(
              book.urlImage,
              fit: BoxFit.fill,
              width: 70,
              height: 70,
            ),
            const SizedBox(
              width: 5,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    book.title,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 15),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(book.author),
                  Text(
                    book.price.toString() + '\$',
                    style: const TextStyle(fontSize: 13),
                  ),
                ],
              ),
            ),
            Consumer<BookBasket>(builder: (context, basket, child) {
              return IconButton(
                  onPressed: () {
                    basket.deleteBook(book);
                  },
                  icon: const Icon(Icons.remove_shopping_cart_sharp));
            }),
          ],
        ),
      );
}
