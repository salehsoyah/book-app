import 'dart:async';

import 'package:book_app/controller/book_controller.dart';
import 'package:book_app/model/book.dart';
import 'package:book_app/model/book_basket.dart';
import 'package:book_app/view/book_cart.dart';
import 'package:book_app/view/book_details.dart';
import 'package:book_app/view/widget/profile.dart';
import 'package:book_app/view/widget/search_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shimmer_animation/shimmer_animation.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final bool _enabled = true;
  List<Book> books = [];
  String query = '';
  Timer? debouncer;

  Future init() async {
    final books = await BooksController.getBooks(query);

    setState(() => this.books = books);
  }

  void debounce(
    VoidCallback callback, {
    Duration duration = const Duration(milliseconds: 1000),
  }) {
    if (debouncer != null) {
      debouncer!.cancel();
    }

    debouncer = Timer(duration, callback);
  }

  Future searchBook(String query) async => debounce(() async {
        final books = await BooksController.getBooks(query);

        if (!mounted) return;

        setState(() {
          this.query = query;
          this.books = books;
        });
      });

  @override
  void initState() {
    init();

    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    debouncer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Profile(
                  name: 'Saleh Souayh',
                  goBack: false,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Row(
                  children: [
                    Expanded(child: buildSearch()),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const BookCart()),
                        );
                      },
                      child: Row(
                        children: [
                          const Icon(Icons.shopping_cart_sharp),
                          Consumer<BookBasket>(
                            builder: (context, basket, child) {
                              return Text(
                                  basket.total.toStringAsFixed(2) + "\$");
                            },
                          ),
                          const SizedBox(
                            width: 16,
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                  child: books.isNotEmpty
                      ? ListView.builder(
                          itemCount: books.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>  BookDetails(book: books[index])),
                                    );
                                  }, child: bookCard(books[index])),
                            );
                          })
                      : Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: shimmer())),
            ],
          ),
        ),
      ),
    );
  }

  Widget shimmer() => Shimmer(
        direction: const ShimmerDirection.fromLTRB(),
        color: const Color.fromRGBO(38, 38, 38, 0.4),
        colorOpacity: 0,
        enabled: _enabled,
        child: ListView.builder(
          itemBuilder: (_, __) => Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: 130.0,
                  height: 70.0,
                  color: Colors.white,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        height: 8.0,
                        color: Colors.white,
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 5.0),
                      ),
                      Container(
                        width: double.infinity,
                        height: 8.0,
                        color: Colors.white,
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 2.0),
                      ),
                      Container(
                        width: 40.0,
                        height: 8.0,
                        color: Colors.white,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          itemCount: 9,
        ),
      );

  Widget buildSearch() => SearchWidget(
        text: query,
        hintText: 'Title or Author Name',
        onChanged: searchBook,
      );

  Widget bookCard(Book book) => Container(
        padding: const EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: const [
            BoxShadow(
              color: Colors.grey,
              offset: Offset(0.0, 1.0), //(x,y)
              blurRadius: 6.0,
            ),
          ],
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.network(
              book.urlImage,
              fit: BoxFit.fill,
              width: 70,
              height: 70,
            ),
            const SizedBox(
              width: 5,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    book.title,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 15),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(book.author),
                  Text(
                    book.price.toString() + '\$',
                    style: const TextStyle(fontSize: 13),
                  ),
                ],
              ),
            ),
            Consumer<BookBasket>(builder: (context, basket, child) {
              return IconButton(
                  onPressed: () {
                    basket.addBook(book);
                  },
                  icon: const Icon(Icons.add_shopping_cart_sharp));
            }),
          ],
        ),
      );
}
