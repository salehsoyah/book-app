import 'dart:convert';
import 'dart:io';
import 'package:book_app/model/book.dart';
import 'package:book_app/model/book_basket.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class BooksController {
  static Future<List<Book>> getBooks(String query) async {
    final url = Uri.parse(
        'https://retoolapi.dev/CLZf5x/books');
    final response = await http.get(url);

    if (response.statusCode == 200) {
      final List books = json.decode(response.body).where((book){
        return book['year'] != null && book['price'] != null;
      }).toList();

      return books.map((json) => Book.fromJson(json)).where((book) {
        final titleLower = book.title.toLowerCase();
        final authorLower = book.author.toLowerCase();
        final searchLower = query.toLowerCase();

        return titleLower.contains(searchLower) ||
            authorLower.contains(searchLower);
      }).toList();
    } else {
      throw Exception();
    }
  }



}
